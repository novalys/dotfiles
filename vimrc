" Required
set nocompatible
filetype off

" New Leader
let mapleader = ","
nmap <leader>s :w<CR>
nmap <leader>d :bd<CR>
nmap <leader>q :q<CR>

" V/H Splits
nmap <leader>v :split<CR>
nmap <leader>> :vsplit<CR>

" Remap ESC
imap jj <Esc>
imap jk <Esc>
inoremap <S-CR> <Esc>

" Remove Trailing WhiteSpace
nnoremap <silent> <F5> :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR> gg

" Change tabs to spaces
nnoremap <silent> <F6> :retab<CR>

" Fix indentation in file
map <F7> mzgg=G`z`

nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

" Enable Vundle
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Plugins
Plugin 'VundleVim/Vundle.vim'
Plugin 'kien/ctrlp.vim'
Plugin 'scrooloose/nerdtree'
Bundle 'jlanzarotta/bufexplorer'
Plugin 'altercation/vim-colors-solarized'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'bling/vim-airline'
Plugin 'jiangmiao/auto-pairs'
Plugin 'mattn/emmet-vim'
Plugin 'pangloss/vim-javascript'
Plugin 'leafgarland/typescript-vim'
Plugin 'sheerun/vim-polyglot'
Plugin 'vim-syntastic/syntastic'
Plugin 'posva/vim-vue'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-git'

" End Plugins
call vundle#end()
filetype plugin indent on

" Themes
syntax enable

" VIM Options
set number
set cursorline

" Airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'

let g:airline_theme='papercolor'
let g:airline_powerline_fonts = 1
set laststatus=2
set noshowmode
set noswapfile

" BuffExplorer
nnoremap <silent> <F12> :BufExplorer<CR>

" Indent by File Type
au Filetype python setl et ts=4 sw=4
au Filetype ruby setl et ts=2 sw=2
au Filetype php setl et ts=4 sw=4
au Filetype javascript setl et ts=2 sw=2
au Filetype typescript setl et ts=2 sw=2
au Filetype html setl et ts=2 sw=2
au Filetype css setl et ts=2 sw=2
au Filetype vue setl et ts=2 sw=2

" VueJS
autocmd BufRead,BufNewFile,BufWinEnter *.vue setf vue

" NerdTree
let NERDTreeIgnore = ['\.pyc$']
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeWinSize=50

" Emmet Vim
let g:user_emmet_install_global = 0
autocmd FileType html,css,php EmmetInstall
let g:user_emmet_leader_key='<C-Z>'

" Ctrl P
let g:ctrlp_working_path_mode = 0

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_python_checkers = ['pylint']
let g:syntastic_python_pylint_args = "--load-plugins pylint_django"

let g:syntastic_php_checkers = ['php', 'phpmd']

let g:syntastic_quiet_messages = { 'level': 'warnings' }
