# Jorge Limas Dot Files

## Vim
File Location:
```
~/.vimrc
```

## i3 Config
File Location:
```
~/.config/i3/config
```
Reference:

- https://www.youtube.com/watch?v=j1I63wGcvU4 (i3 Configuration Tutorial 1/3)
- https://www.youtube.com/watch?v=8-S0cWnLBKg (i3 Configuration Tutorial 2/3)
- https://www.youtube.com/watch?v=ARKIwOlazKI (i3 Configuration Tutorial 3/3)

## i3 Blocks
File Location
```
~/.config/i3/i3blocks.conf
```
